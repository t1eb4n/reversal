const reversal = require('../../lib/reversal');

const errorMock = '../mocks/codeError';
const testWorkAround = () => {
  return reversal.require(errorMock);
};

describe('Reversal', () => {
  beforeEach(() => {
    reversal.reset();
  });

  it('should throw an exception on non-existant require', () => {
    const nonExistantModule = 'iDoNotExist';

    expect(reversal.require.bind(reversal, nonExistantModule)).toThrow(`Unfound module: ${nonExistantModule}`);
  });

  it('should alert you to a module error', () => {
    try {
      testWorkAround();
      fail();
    } catch(err) {
      expect(err).toEqual(`Error in module: ${errorMock} - ReferenceError: modules is not defined`);
    }
  });

  it('should be able to detect calling file', () => {
    expect(reversal.getCallingFile()).toEqual(__filename);
  });

  it('should return things from require if not found internally', () => {
    const assert = require('assert');

    expect(reversal.require('assert')).toEqual(assert);
  });

  it('should return itself on registering module', () => {
    expect(reversal.register('assert', {})).toEqual(reversal);
  });

  it('should override existing modules', () => {
    const newAssert = {};
    reversal.register('assert', newAssert);

    expect(reversal.require('assert')).toEqual(newAssert);
  });

  it('should start include path location of calling file', () => {
    expect(reversal.require('../../package.json').name).toEqual('reversal');
  });

  it('should allow for creating config options', () => {
    expect(reversal.setConfig('ENV', 'development')).toEqual(reversal);
  });

  it('should default to null for non-defaulted config options', () => {
    expect(reversal.getConfig('ENV')).toEqual(null);
  });

  it('should return default for non-existant config options', () => {
    expect(reversal.getConfig('ENV', 'development')).toEqual('development');
  });

  it('should check environment variables for config options', () => {
    const configValue = 'IWork';
    process.env.FOR_TESTING = configValue;

    expect(reversal.getConfig('FOR_TESTING')).toEqual(configValue);
    delete process.env.FOR_TESTING;
  });

  it('should return registered config options', () => {
    reversal.setConfig('ENV', 'development');

    expect(reversal.getConfig('ENV')).toEqual('development');
  });

  it('should allow you to reset', () => {
    reversal.setConfig('RESET_TEST', 'value');
    reversal.reset();

    expect(reversal.getConfig('RESET_TEST')).toBeNull();
  });

  it('should clear require cache on reset', () => {
    require('../../lib/reversal');
    require('../mocks/workingModule');

    expect(Object.keys(require.cache).length).toBe(2);
    reversal.reset();

    /**
     * This is 1 because reversal is not cleared from the require cache
     */
    expect(Object.keys(require.cache).length).toBe(1);
  });

  it('should throw error for undefined modules when overloading only', () => {
    try {
      reversal.require('iDoNotExist', true);
      fail('Should throw error with unknown module while overloading');
    } catch(err) {
      expect(err).toBe('Unfound module: iDoNotExist');
    }
  });
});
