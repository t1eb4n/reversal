const path             = require('path');

/**
 * Inversion of control, and configuration management helper.
 * 
 * @class
 * @public
 */
class Reversal {
  constructor() {
    this.config      = {};
    this.modules     = {};
  }

  /**
   * Get a stored configuration value or a defeult value
   * 
   * @param   {string} configName   Configuration name
   * @param   {*}      defaultValue Default value to return if configuration does not exist.
   * @returns {*}      Stored configuration value
   */
  getConfig(configName, defaultValue = null) {
    return this.config[configName.toString()] || process.env[configName.toString()] || defaultValue;
  }

  /**
   * Set a configuration value
   * 
   * @param   {string}   configName  Configuration name
   * @param   {*}        configValue Configuration value
   * @returns {Reversal}             This Reversal instance
   */
  setConfig(configName, configValue) {
    this.config[configName.toString()] = configValue;
    return this;
  }

  /**
   * Register a module for requiring later.
   * 
   * @see     {@link Reversal#require}
   * @param   {string} moduleName Module name
   * @param   {*}      component  Component to be returned
   * @returns {Reversal}          This Reversal instance
   */
  register(moduleName, component) {
    this.modules[moduleName.toString()] = component;
    return this;
  }

  /**
   * Require a module. Works just like Node's default require but first checks
   * the internal modules object before falling back to Node's require.
   * 
   * @throws                            Unable to find requested module.
   * @throws                            Error in the module being required.
   * @param   {string}  moduleName      Module name to require.
   * @param   {boolean} skipNodeRequire Only return from internal module object
   * @returns {*}                       Either locally registered module or returned value from Node's require
   */
  require(moduleName, skipNodeRequire) {
    if('undefined' !== typeof this.modules[moduleName.toString()]) {
      return this.modules[moduleName.toString()];
    }

    if(skipNodeRequire) {
      throw `Unfound module: ${moduleName}`;
    }

    try {
      if(0 === moduleName.indexOf('.')) {
        const fileName = path.join(path.dirname(this.getCallingFile()), moduleName);
        return require(fileName);
      }

      return require(moduleName);
    } catch (e) {
      if('MODULE_NOT_FOUND' === e.code) {
        throw `Unfound module: ${moduleName}`;
      }

      throw `Error in module: ${moduleName} - ${e}`;
    }
  }

  /**
   * Get file that called require. This is used to get the relative path from
   * where the require is happening from.
   * 
   * Shamefully admitting this was stolen almost verbatum from:
   * https://stackoverflow.com/a/29581862
   * 
   * @returns {string} File name of calling file.
   */
  getCallingFile() {
    const originalFunc = Error.prepareStackTrace;

    let callerfile;
    try {
      const err = new Error();
      let currentfile;

      Error.prepareStackTrace = function (err, stack) { return stack; };

      currentfile = err.stack.shift().getFileName();
      while (err.stack.length) {
        callerfile = err.stack.shift().getFileName();

        if(currentfile !== callerfile) break;
      }
    } catch (e) {
      // Continue even if there is an error.
    }

    Error.prepareStackTrace = originalFunc;
    return callerfile;
  }

  /**
   * Reset all internal configuration and module definitions and clear Node's
   * internal require cache, but leave the cached version on Reversal. This
   * saves from reloading reversal in unit tests causing the unit test and the 
   * library being tested from using two different versions of Reversal.
   * 
   * @returns {Reversal} This Reversal instance.
   */
  reset() {
    // (Almost) Fully clear nodes require cache
    Object.keys(require.cache)
      .forEach(module => (__filename !== module) && delete require.cache[module.toString()]);

    this.config   = {};
    this.modules  = {};

    return this;
  }
}

module.exports = new Reversal();
