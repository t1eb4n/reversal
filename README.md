# reversal

An inversion of control object to make for easier dependency injection and code testing.

## Installation

```Bash
npm install --save reversal
```

## Usage
### Registering object
```JavaScript
const reversal = require('reversal');
reversal.register('simpleObject', {foo: 'bar'});
```

### Requiring object
Please note all requires start at the root of the project, not from the location
of the current file.

```JavaScript
// Basic include
const http = reversal.require('http'); // Same as const http = require('http');

// Loading self registered objects
const test = reversal.require('simpleObject');
console.log(test.foo); // bar

// Exceptions
const error = reversal.require('iDoNotExist'); // Throws Unfound module: iDoNotExist

```

### Configuration storage
Reversal gives you a location to store configuration options for your application.
It will look in `process.env` for the requested value if you have not configured one.
You also have the option of providing a fall back value in the instance the requested
configuration option is not available.

```Javascript
// Set
reversal.setConfig('TESTING_OPTION', 'foo');

// Get, assuming process.env.SOME_VALUE = 'test', and process.env.ANOTHER_VALUE doesn't exist
console.log(reversal.getConfig('SOME_VALUE')); // test
console.log(reversal.getConfig('ANOTHER_VALUE')); // NULL
console.log(reversal.getConfig('ANOTHER_VALE', 'defaultValue')); // defaultValue
```

### Reset
During testing it may become necessary to reset the Reversal object to a fresh state.
This is done simply with `reversal.reset();`. This will remove all registered configurations,
and objects.
