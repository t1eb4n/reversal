module.exports = {
  'env': {
    'es2021': true,
    'node': true
  },
  'extends': [
    'eslint:recommended',
    'plugin:jsdoc/recommended',
    'plugin:security/recommended'
  ],
  'ignorePatterns': ['jsdocs'],
  'parserOptions': {'ecmaVersion': 12},
  'plugins': [
    'jsdoc',
    'security'
  ],
  'rules': {
    'indent':          ['error', 2],
    'linebreak-style': ['error', 'unix'],
    'quotes':          ['error', 'single'],
    'semi':            ['error', 'always'],
    'no-var':          ['error'],

    /**
     * JSDocs Rules
     */
    'jsdoc/require-description': 1,
    'jsdoc/require-throws':      1,

    /**
     * Security Rules
     */
    'security/detect-non-literal-require': 0
  }
};